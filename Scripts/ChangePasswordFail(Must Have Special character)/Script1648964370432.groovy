import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.junit.Assert as Assert

WebUI.openBrowser('')

WebUI.navigateToUrl('https://myid.buu.ac.th/')

WebUI.setText(findTestObject('Object Repository/ChangePasswordFail(Must Have Special character)/Page_My ID/input_(Username)_user'), 
    '62160132')

WebUI.setEncryptedText(findTestObject('Object Repository/ChangePasswordFail(Must Have Special character)/Page_My ID/input_(Password)_pass'), 
    'D2z9VZoguRlmkm/8CH0mlA==')

WebUI.click(findTestObject('Object Repository/ChangePasswordFail(Must Have Special character)/Page_My ID/button_Sign in'))

WebUI.click(findTestObject('Object Repository/ChangePasswordFail(Must Have Special character)/Page_My ID/a_Change PasswordRenew password'))

WebUI.setEncryptedText(findTestObject('Object Repository/ChangePasswordFail(Must Have Special character)/Page_My ID/input_(New Password)_newpass'), 
    '6F5Mcm/Tk+LDCElR5yXY0Q==')

WebUI.setEncryptedText(findTestObject('Object Repository/ChangePasswordFail(Must Have Special character)/Page_My ID/input_(Re-New Password)_renewpass'), 
    '6F5Mcm/Tk+LDCElR5yXY0Q==')

WebUI.click(findTestObject('Object Repository/ChangePasswordFail(Must Have Special character)/Page_My ID/button_Change Password'))

AccResult = WebUI.getUrl()

ExpectResult = 'https://myid.buu.ac.th/profile/chgpwdlogin'

Assert.assertEquals(ExpectResult, AccResult)

WebUI.closeBrowser()

